import { Student } from './student.class';

// Section class to represeent a course section and its students
export class Section {

    constructor(public name: String, public students: Array<Student>) { }
    
}