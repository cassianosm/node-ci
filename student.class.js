"use strict";
exports.__esModule = true;
//Student class to represent students
var Student = /** @class */ (function () {
    function Student(name) {
        this.name = name;
    }
    return Student;
}());
exports.Student = Student;
