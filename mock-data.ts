import { Student } from './student.class';
import { Section } from './section.class';

// Mock data
export const SECTION01: Section = new Section("Section01", [new Student("Cassiano")]);