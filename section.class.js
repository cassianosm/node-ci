"use strict";
exports.__esModule = true;
// Section class to represeent a course section and its students
var Section = /** @class */ (function () {
    function Section(name, students) {
        this.name = name;
        this.students = students;
    }
    return Section;
}());
exports.Section = Section;
