"use strict";
exports.__esModule = true;
var student_class_1 = require("./student.class");
var section_class_1 = require("./section.class");
// Mock data
exports.SECTION01 = new section_class_1.Section("Section01", [new student_class_1.Student("Cassiano")]);
