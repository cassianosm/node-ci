import {} from 'jasmine';
import { Student } from './student.class';
import { Section } from './section.class';
import { SECTION01 } from './mock-data';

describe('Student Suite', function () {

    it('Create student', function () {
        let std = new Student('John');
        expect(std.name).toBe('John');
    });
});

describe('Section Suite', function () {

    let section: Section = new Section("Section001", [new Student("Mary")]);

    it('Section Name', function () {
        expect(section.name).toBe('Section001');
    });

});

describe('Students in Section Suite', function () {

    it('Student Cassiano', function () {
        expect(SECTION01.students).toContain(new Student('Cassiano'));
    });

});