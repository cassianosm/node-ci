"use strict";
exports.__esModule = true;
var student_class_1 = require("./student.class");
var section_class_1 = require("./section.class");
var mock_data_1 = require("./mock-data");
describe('Student Suite', function () {
    it('Create student', function () {
        var std = new student_class_1.Student('John');
        expect(std.name).toBe('John');
    });
});
describe('Section Suite', function () {
    var section = new section_class_1.Section("Section001", [new student_class_1.Student("Mary")]);
    it('Section Name', function () {
        expect(section.name).toBe('Section001');
    });
});
describe('Students in Section Suite', function () {
    it('Student Cassiano', function () {
        expect(mock_data_1.SECTION01.students).toContain(new student_class_1.Student('Cassiano'));
    });
});
